package com.example.opaw.simplefragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class FormFragment extends Fragment {

    private View rootView;

    public FormFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_form, container, false);

        TextView lblFragmentForm = (TextView) rootView.findViewById(R.id.lbl_fragment_form);
        lblFragmentForm.setText("Ini Fragment Form");

        return rootView;
    }

}
