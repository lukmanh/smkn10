package com.example.opaw.simplefragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ListFragment extends Fragment {

    private View rootView;

    public ListFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_list, container, false);

        TextView lblFragmentList = (TextView) rootView.findViewById(R.id.lbl_fragment_list);
        lblFragmentList.setText("Ini Fragment List");

        return rootView;
    }

}
