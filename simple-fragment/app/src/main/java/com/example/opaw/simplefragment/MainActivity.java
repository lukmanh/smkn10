package com.example.opaw.simplefragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    FormFragment formFragment = new FormFragment();
    ListFragment listFragment = new ListFragment();
    FragmentTransaction transaction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnForm = (Button) findViewById(R.id.btn_form);
        Button btnList = (Button) findViewById(R.id.btn_list);

        transaction = getSupportFragmentManager().beginTransaction();

        btnForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFragment(formFragment);
            }
        });

        btnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFragment(listFragment);
            }
        });

    }

    private void openFragment(Fragment fragment)   {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
